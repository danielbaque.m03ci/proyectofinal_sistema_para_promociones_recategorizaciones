const form = document.getElementById('form');
const names = document.getElementById('names');
const usuario = document.getElementById('username');
const Tell = document.getElementById('Tell');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');

// Medoto para evento enviar
form.addEventListener('submit', e => {
	e.preventDefault();

	checkInputs();
});
// validacion de inputs
function checkInputs() {

	const namesValue = names.value.trim();
	const usuarioValue = usuario.value.trim();
	const TellValue = Tell.value.trim();
	const emailValue = email.value.trim();
	const passwordValue = password.value.trim();
	const password2Value = password2.value.trim();

	if (namesValue === '') {
		setErrorFor(names, 'No puede dejar este campo en blanco');
	} else if (names.value.length < 4 || names.value.length > 40) {
		setErrorFor(names, 'Nombre no válido');
	} else {
		setSuccessFor(names);
	}

	if (usuarioValue === '') {
		setErrorFor(usuario, 'No puede dejar este campo en blanco');
	} else if (usuario.value.length < 5 || usuario.value.length > 15) {
		setErrorFor(usuario, 'Nombre de Usuario válido');
	} else {
		setSuccessFor(usuario);
	}

	if (TellValue === '') {
		setErrorFor(Tell, 'No puede dejar este campo en blanco');
	} else if (Tell.value.length < 5 || Tell.value.length > 10) {
		setErrorFor(Tell, 'Numero telefonico no válido, el numero debe contener 10 digitos');
	} else {
		setSuccessFor(Tell);
	}

	if (emailValue === '') {
		setErrorFor(email, 'No puede dejar este campo en blanco');
	} else if (!isEmail(emailValue)) {
		setErrorFor(email, 'No ingreso un email válido');
	} else {
		setSuccessFor(email);
	}

	if (passwordValue === '') {
		setErrorFor(password, 'No puede dejar este campo en blanco');
	} else if (password.value.length < 5 || password.value.length > 30) {
		setErrorFor(password, 'Contraseña no válida, es muy corta');
	} else {
		setSuccessFor(password);
	}

	if (password2Value === '') {
		setErrorFor(password2, 'No puede dejar este campo en blanco');
	} else if (passwordValue !== password2Value) {
		setErrorFor(password2, 'Las contraseñas no coinciden');
	} else {
		setSuccessFor(password2);
	}

}

// funcion para los mensajes de error
function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

// funcion para verificar campos correctos 
function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}

// funcion para verificar el email
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}

/// funcion para permitir solo letras 

function letrass(e) {
	key = e.keyCode || e.which;
	tecla = String.fromCharCode(key);
	letras = " ABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúabcdefghijklmnñopqrstuvwxyz";
	especiales = "8-37-39-46";
	tecla_especial = false;
	for (var i in especiales) {
		if (key == especiales[i]) {
			tecla_especial = true;
			break;
		}
	}
	if (letras.indexOf(tecla) == -1 && !tecla_especial) {
		return false;
	}
}


/// funcion para permitir solo numeros
function numeros(evt) {
	var validar = (evt.which) ? evt.which : evt.keyCode;
	if (validar == 8) {
		return true;
	} else if (validar >= 48 && validar <= 57) {
		return true;
	} else {
		return false;
	}
}

