const form = document.getElementById('form');
const nombres = document.getElementById('names');
const cedula = document.getElementById('cedula');
const senecyt = document.getElementById('senecyt');
const telefono = document.getElementById('telefono');
const email = document.getElementById('email');
const cargoa = document.getElementById('cargoa');
const fecha = document.getElementById('fecha');
const documentos = document.getElementById('documentos');
const titulos = document.getElementById('titulos');
const certificados = document.getElementById('certificados');

//URL de la API
const API_URL = "http://localhost:3050"

// Metodo para evento enviar
form.addEventListener('submit', e => {
	e.preventDefault();    
	const res = checkInputs();	
	var str = JSON.stringify(documentos.files[0]);
	var bytes = new TextEncoder().encode(str);
	const blobDocumentos = new Blob([bytes], {
		type: "application/pdf"
	});	
	var str = JSON.stringify(titulos.files[0]);
	var bytes = new TextEncoder().encode(str);
	const blobTitulos = new Blob([bytes], {
		type: "application/pdf"
	});	
	var str = JSON.stringify(certificados.files[0]);
	var bytes = new TextEncoder().encode(str);
	const blobCertificados = new Blob([bytes], {
		type: "application/pdf"
	});	
	if(res==0){        
		const jsonFormulario = {
            "nombreCompleto": nombres.value,
            "cedula": cedula.value,
            "senecyt": senecyt.value,
            "telefono": telefono.value,
            "email": email.value,
            "cargoActual": cargoa.value,
            "fechaRegistro": fecha.value,
            "documentos": blobDocumentos,
            "titulos": blobTitulos,
            "certificados": blobCertificados
        }		
        fetch(`${API_URL}/formularioRecategorizacion`, {
            method: 'POST',
            headers: {               
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(jsonFormulario)
        })
            .then(response => response.text())
            .then(text => {
                alert(text)
                location.reload()
            })
	}else{
		alert("No puede continuar si hay campos incorrectos o vacios")
	}		
});

// validacion de inputs
function checkInputs() {
	
	var verificador = 0

	if (nombres.value === '') {
		setErrorFor(nombres, 'No puede dejar este campo en blanco');
		verificador++
	} else if (nombres.value.length < 4 || nombres.value.length > 40) {
		setErrorFor(nombres, 'Nombre no válido');
		verificador++
	} else {
		setSuccessFor(nombres);
	}

	if (cedula.value === '') {
		setErrorFor(cedula, 'No puede dejar este campo en blanco');
		verificador++
	} else if (cedula.value.length < 5 || cedula.value.length > 10) {
		setErrorFor(cedula, 'Número de cédula no es válido');
		verificador++
	} else {
		setSuccessFor(cedula);
	}

	if (telefono.value === '') {
		setErrorFor(telefono, 'No puede dejar este campo en blanco');
		verificador++
	} else if (telefono.value.length < 5 || telefono.value.length > 10) {
		setErrorFor(telefono, 'Número telefonico no válido, el numero debe contener 10 digitos');
		verificador++
	} else {
		setSuccessFor(telefono);
	}

	if (email.value === '') {
		setErrorFor(email, 'No puede dejar este campo en blanco');
		verificador++
	} else if (!isEmail(email.value)) {
		setErrorFor(email, 'No ingreso un email válido');
		verificador++
	} else {
		setSuccessFor(email);
	}	

    if (senecyt.value === '') {
		setErrorFor(senecyt, 'No puede dejar este campo en blanco');
		verificador++
	} else {
		setSuccessFor(senecyt);
	}

    if (cargoa.value === '') {
		setErrorFor(cargoa, 'No puede dejar este campo en blanco');
		verificador++
	} else {
		setSuccessFor(cargoa);
	}

    if (fecha.value === '') {
		setErrorFor(fecha, 'No puede dejar este campo en blanco');
		verificador++
	} else {
		setSuccessFor(fecha);
	}

    if (documentos.value === '') {
		setErrorFor(documentos, 'No puede dejar este campo en blanco');
		verificador++
	} else {
		setSuccessFor(documentos);
	}

    if (titulos.value === '') {
		setErrorFor(titulos, 'No puede dejar este campo en blanco');
		verificador++
	} else {
		setSuccessFor(titulos);
	}

    if (certificados.value === '') {
		setErrorFor(certificados, 'No puede dejar este campo en blanco');
		verificador++
	} else {
		setSuccessFor(certificados);
	}

	return verificador

}


// funcion para los mensajes de error
function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

// funcion para verificar campos correctos 
function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}

// funcion para verificar el email
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}

/// funcion para permitir solo letras 
function letrass(e) {
	key = e.keyCode || e.which;
	tecla = String.fromCharCode(key);
	letras = " ABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúabcdefghijklmnñopqrstuvwxyz";
	especiales = "8-37-39-46";
	tecla_especial = false;
	for (var i in especiales) {
		if (key == especiales[i]) {
			tecla_especial = true;
			break;
		}
	}
	if (letras.indexOf(tecla) == -1 && !tecla_especial) {
		return false;
	}
}

/// funcion para permitir solo numeros
function numeros(evt) {
	var validar = (evt.which) ? evt.which : evt.keyCode;
	if (validar == 8) {
		return true;
	} else if (validar >= 48 && validar <= 57) {
		return true;
	} else {
		return false;
	}
}