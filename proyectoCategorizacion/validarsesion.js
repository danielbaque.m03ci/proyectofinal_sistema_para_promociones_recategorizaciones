const usuario = document.getElementById("correoo");
const contrase = document.getElementById("contrase");
const form = document.getElementById("form");
const parrafo = document.getElementById("warnings");

//URL de la API
const API_URL = "http://localhost:3050"

form.addEventListener("submit", e => {
    e.preventDefault();
    const res = checkInputs();
    if (res == 0) {
        const jsonLogin = {
            "email": usuario.value,
            "clave": contrase.value
        }
        fetch(`${API_URL}/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(jsonLogin)
        })
            .then(response => response.text())
            .then(json => {                 
                if(json == "vacio"){
                    alert("Las credenciales son incorrectas, inténtelo de nuevo")
                    usuario.value = ""
                    contrase.value = ""
                }else{
                    const datos = JSON.parse(json)       
                    sessionStorage.setItem("idUsuario", datos[0]['id'])             
                    alert("Bienvenido " + datos[0]['nombre'])  
                    location.href = "Home.html"                  
                }          
            })
    }
})

function checkInputs() {
    var verificador = 0
    if (usuario.value === '') {
        alert('No puede dejar este campo en blanco');
        verificador++
    } else if (!isEmail(usuario.value)) {
        alert('No ingreso un email válido');
        verificador++
    }
    if (contrase.value === '') {
        alert('No puede dejar este campo en blanco');
        verificador++
    } else if (contrase.value.length < 5 || contrase.value.length > 30) {
        alert('Contraseña no válida, es muy corta');
        verificador++
    }
    return verificador
}

// funcion para verificar el email
function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}
