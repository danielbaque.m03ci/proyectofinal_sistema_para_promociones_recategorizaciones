//URL de la API
const API_URL = "http://localhost:3050";

const tabla = document.getElementById("tblPromociones");

fetch(`${API_URL}/promociones`, {
    method: 'GET',
})
    .then(response => response.text())
    .then(json => {
        if (json == "vacio") {
            alert("No hay formularios registrados para Promociones")
        } else {
            const datos = JSON.parse(json)
            for (var x = 0; x < datos.length; x++) {
                var opciones = { year: 'numeric', month: 'short', day: 'numeric' };
                var fecha = new Date(datos[x]['fechaRegistro'])
                    .toLocaleDateString('es', opciones)
                    .replace(/ /g, '-')
                    .replace('.', '')
                    .replace(/-([a-z])/, function (x) { return '-' + x[1].toUpperCase() });                 
                tabla.insertRow(-1).innerHTML = "<tr><td>" + datos[x]['id'] + "</td><td>" + datos[x]['nombreCompleto'] + "</td><td>" + datos[x]['cedula'] + "</td><td>" + datos[x]['senecyt'] + "</td><td>" + datos[x]['telefono'] + "</td><td>" + datos[x]['email'] + "</td><td>" + datos[x]['cargoActual'] + "</td><td>" + datos[x]['cargoPostula'] + "</td><td>" + fecha + "</td></tr>"
            }
        }
    })